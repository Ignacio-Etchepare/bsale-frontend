const BASE_URL = 'https://bsale-ignacio-etchepare.herokuapp.com'

async function obtenerCategorias() {
    try {
        let categorias = []


        const result = await axios.get(`${BASE_URL}/categoria/listado`);

        if (result.data && result.data.data) {
            categorias = result.data.data
        }

        const select = $('#select-categorias');
        for (let categoria of categorias) {
            select.append(`
                <option value="${categoria.id}">${categoria.name}</option>
            `)
        }
    } catch (error) {
        console.log(error);
    }
}


async function obtenerProductos() {
    try {
        const nombre = $('#search-input').val();
        const categoria = $('#select-categorias').val();
        console.log({ categoria })
        let categorias = {}


        const result = await axios.get(`${BASE_URL}/producto/listado`, { params: { 'nombre': nombre, 'categoriaId': categoria } });

        let data = [];
        let total = 0;
        if (result.data && result.data.data) {
            data = result.data.data
            total = result.data.total;
        }

        // Selector de categorias
        const container = $('#container-categorias');
        container.empty();

        // Selector de error
        const errorMensaje = $('#container-categorias');
        errorMensaje.empty();

        if (total == 0) {
            // Se agrega mensaje de error
            errorMensaje.append(`
            <div class="alert alert-warning border border-warning text-center">
                No hay resultados en la busqueda solicitada con el nombre <b>'${nombre}'</b>
            </div>
            `)

            return;
        }

        const defaultImagen = "http://www.redcomingenieria.cl/images/no-imagen.jpg"


        
        for (let item of data) {
            if (!categorias.hasOwnProperty(item.category_name)) {
                categorias[item.category_name] = []
            }

            categorias[item.category_name].push(item)
        }

        for (let categoria of Object.keys(categorias)) {
            const titulo = `
            <h3 class="font-weight-bold mt-3">${String(categoria).toUpperCase()}</h3>
            <hr class="my-2">
            `;
            
            let productos = ''

            for (let producto of categorias[categoria]) {
                let imagen = `<img class="img-fluid image-product" src="${producto.url_image ? producto.url_image : defaultImagen}" alt="Imágen producto">`;
                let discountBarra = '';
                if (producto.discount > 0) {
                    discountBarra = `
                        <div class="position-absolute w-100">
                            <div class="barra-descuento">
                                ${producto.discount}% DESC.
                            </div>
                        </div>
                    `
                }
                productos += `
                
                <div class="col-md-3 mt-3">
                    <div class="card shadow-sm">
                        ${discountBarra}
                        <div class="card-body text-center">
                            ${imagen}
                        <br>
                            ${producto.name}
                        </div>
                        <div class="card-footer d-flex justify-content-between bg-white">
                            <div class="my-auto">
                                $ ${formatearNumero(Math.round(producto.price * ((100 - producto.discount) / 100)))}
                            </div>
                            <div>
                                <button class="btn btn-secondary rounded-circle">
                                    <i class="fas fa-cart-plus fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                `;
            }


            container.append(titulo)
            container.append(`
                <div class="row">
                    ${productos}
                </div>
            `)

        }


            
        

        

        console.log(categorias)
    } catch (error) {
        console.log(error);
    }
}

// Se formatea el separador de millares
function formatearNumero(numero) {
    return String(numero).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}

async function main() {
    await obtenerCategorias();
    await obtenerProductos();
};

$(document).ready(function () {
    $('#form-search').submit(e => {
        e.preventDefault();
        obtenerProductos();
    })
});

main();