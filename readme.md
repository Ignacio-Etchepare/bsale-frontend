# Test de Postulación para BSale
## **Ignacio Etchepare** 

Proyecto frontend de sistema de carro de compras de productos.

Se crea una aplicación con Javascript Vanilla, HTML y CSS. La aplicación es un listado de productos filtrados por nombre y categoría.

## Uso

Abrir archivo **index.html** en el navegador.

## Plugins utilizados
- **Font awesome** v6.0.0-beta3
- **Bootstrap** v5.1.3
- **Axios** v0.24.0
- **jQuery** v3.6.0

# Deploy

El deploy se encuentra alojado en Heroku y se accede por medio del siguiente enlace:

**[https://bsale-frontend-ietchepare.herokuapp.com](https://bsale-frontend-ietchepare.herokuapp.com)**


Se utiliza plataforma GitLab para los repositorios de backend y de frontend del proyecto:

### Backend
[https://gitlab.com/Ignacio-Etchepare/backend-bsale](https://gitlab.com/Ignacio-Etchepare/backend-bsale)


### Frontend
[https://gitlab.com/Ignacio-Etchepare/bsale-frontend](https://gitlab.com/Ignacio-Etchepare/bsale-frontend)

